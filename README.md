## Author: Ahmed Al Ali, aalali@uoregon.edu ##

The pageserver.py located in the directory pageserver was edited to handle different scenarios.

a) GET name.html, name.css
b) Return 404 if the file was not found in the directory pages
c) Return 403 if there were forbidden characters. (~, //, /..)

All implementations have been tested by using the tests shell located in the tests folder.

All tests were passed.